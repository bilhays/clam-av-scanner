Scripts for clamav

This should work for ubuntu, os x and redhat. The os x bits assume you're using brew or clamXav, which is a wonderful program, I highly recommend it, see 
http://www.clamxav.com/

The purpose of these scripts is to automate scanning with clamav. I played
with the clamd and didn't like it much so I did this instead. This stuff does nothing to clean up problems, it just looks for them and notifies you. 

You should be able to rsync the tree to a box and have it all just work
after tweaking the configuration file for email settings.

Files include:
clamcron, a wrapper that calls the clamavscanner script as a weekly cronies
clamav-scan and calmav-freshclam which handle log rotation
check_clam.cfg, a conf file for the nrpe plugin
check_clam, a nrpe script that runs the check_clam_mailer script. We run
this every 15 minutes, so you can poll your system via nagios if you want.
You will need to give nagios/nrpe sudoer privs.
check_clam_mailer, a script that checks the clam logs for freshness and
looks for infections, and sends an email if there's a problem. Remember,
the dead cannot call you, so if you're not running nagios you'll want
another process to make sure this is running ok.
clamavscanner, this runs the scans.

Use at your own risk, I can barely make bits move and cannot be held
responsibly for infections, malware, floods, fire, famine or crashes
resulting from the use of this software. 
